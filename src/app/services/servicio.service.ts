import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ClienteModel } from '../models/cliente.model';
import { PuntosModel } from '../models/puntos.model';
import { ReglaModel } from '../models/reglas.model';
import { ParametroModel } from '../models/parametro.model';
import { UsoPuntosModel } from '../models/usoPuntos.model';
import { BolsaModel } from '../models/bolsa.model';



@Injectable({
  providedIn: 'root'
})
export class ServicioService {

  private urlTp = 'http://gy7228.myfoscam.org:8080/rest';

  constructor(private http: HttpClient) { }

/* mostrar los clientes */
  getClientes() {
    return this.http.get(`${this.urlTp}/clientes/all`)
      .pipe(
        map(this.crearArregloClientes),

      );
  }

  private crearArregloClientes(clientesObj) {

    const clients: ClienteModel[] = [];

    //console.log('lo que trae del puto array');
    //console.log(categoriasObj);


    if (clientesObj === null) { return []; }
    if (clientesObj.data.clientes === null) { return []; }

    clientesObj.data.clientes.forEach((item) => {

      const cliente = new ClienteModel();
      cliente.id = item.id;
      cliente.nombre = item.nombre;
      cliente.apellido = item.apellido;
      cliente.fechaCaducidad = item.fechaCaducidad;
      cliente.telefono = item.telefono;
      cliente.nroDocumento = item.nroDocumento;
      cliente.email = item.email;
      
      clients.push(cliente);
    });
      //console.log("desde servicio", clients);
    return clients;

  }

/* guardar un cliente **/
setCliente(nombre, apellido, nroDocumento, email, telefono, fecha) {

  /* body */
  const body = {
    "nombre": nombre,
    "apellido": apellido,
    "nroDocumento": nroDocumento,
    "email": email,
    "telefono": telefono,
    "fechaNacimiento" : fecha,
    
  };
  

  return this.http.post(`${this.urlTp}/clientes/add`, body)
    .pipe(
      map((resp: any) => {

        console.log(resp);
        return resp;
      })
    );



}

/* paginar clientes */
paginarClientes(parametro, startIndexPage, pageSize){

  let params = new HttpParams();
    params = params.append('parametro', `${parametro}`);


  /* body */
  const body = {
    "startIndexPage": startIndexPage,
    "pageSize": pageSize,
    
    
  };

  return this.http.post(`${this.urlTp}/clientes/getByPage`, body, { params: params })
  .pipe(
    map(this.crearArregloClientes),
  );

  
}

/* mostrar vales */

getVales() {
  return this.http.get(`${this.urlTp}/vales/all`)
    .pipe(
      map(this.crearArregloVales),

    );
}

private crearArregloVales(valesObj) {

  const vals: PuntosModel[] = [];

  //console.log('lo que trae del puto array');
  //console.log(categoriasObj);


  if (valesObj === null) { return []; }
  if (valesObj.data.vales === null) { return []; }

  valesObj.data.vales.forEach((item) => {

    const vale = new PuntosModel();
    vale.id = item.id;
    vale.descripcion= item.descripcion;
    vale.cantidadRequerida = item.cantidadRequerida;
    
    vals.push(vale);
  });
    //console.log("desde servicio", clients);
  return vals;

}

/* guardar un vale **/
setVale(descripcion, cantidadRequerida) {

  /* body */
  const body = {
    "descripcion": descripcion,
    "cantidadRequerida": cantidadRequerida,
    
  };
  

  return this.http.post(`${this.urlTp}/vales/add`, body)
    .pipe(
      map((resp: any) => {

        console.log(resp);
        return resp;
      })
    );



}

/* paginacion de vales */
paginarVales(startIndexPage, pageSize){

  let params = new HttpParams();
    params = params.append('page', `${startIndexPage}`);
    params = params.append('size', `${pageSize}`);

  /* body */
  const body = {
    "startIndexPage":startIndexPage,
    "pageSize":pageSize
    
    
  };

  console.log(params);

  return this.http.get(`${this.urlTp}/vales/paginado`, {params : params})
  .pipe(
    map(this.crearArregloVales),
  );

}

/* mostrar las reglas */

getReglas() {
  return this.http.get(`${this.urlTp}/reglas/all`)
    .pipe(
      map(this.crearArregloReglas),

    );
}

private crearArregloReglas(reglasObj) {

  const reglas: ReglaModel[] = [];

  //console.log('lo que trae del puto array');
  //console.log(categoriasObj);


  if (reglasObj === null) { return []; }

  reglasObj.data.reglas.forEach((item) => {

    const regla = new ReglaModel();
    regla.id = item.id;
    regla.limiteInferior= item.limiteInferior;
    regla.limiteSuperior = item.limiteSuperior;
    regla.montoEquivalencia = item.montoEquivalencia;

    
    reglas.push(regla);
  });
    //console.log("desde servicio", clients);
  return reglas;

}

/* guardar regla */
setRegla(limiteInferior, limiteSuperior, montoEquivalencia){
  /* body */
  const body = {
    "montoEquivalencia": montoEquivalencia,
    "limiteInferior": limiteInferior,
    "limiteSuperior": limiteSuperior,
    
  };
  

  return this.http.post(`${this.urlTp}/reglas/add`, body)
    .pipe(
      map((resp: any) => {

        console.log(resp);
        return resp;
      })
    );
}

actualizarRegla(idRegla,montoEquivalencia, limiteInferior, limiteSuperior){

  /* body */
  const body = {
    "montoEquivalencia": montoEquivalencia,
    "limiteInferior": limiteInferior,
    "limiteSuperior": limiteSuperior,
    
  };
  

  return this.http.put(`${this.urlTp}/reglas/edit/${idRegla}`, body)
    .pipe(
      map((resp: any) => {

        console.log(resp);
        return resp;
      })
    );

}

buscarRegla(idRegla){

  return this.http.get<ReglaModel>(`${this.urlTp}/reglas/id/${idRegla}`);

}

buscarParametro(idPrametro){

  return this.http.get<ParametroModel>(`${this.urlTp}/parametros/id/${idPrametro}`);

}

setParametro(fechaInicioValidez, fechaFinValidez, diasDuracion){

  const body = {
    "fechaInicioValidez":fechaInicioValidez,
    "fechaFinValidez": fechaFinValidez ,
      "diasDuracion": diasDuracion
    
  };
  

  return this.http.post(`${this.urlTp}/parametros/add`, body)
    .pipe(
      map((resp: any) => {

        console.log(resp);
        return resp;
      })
    );

}

getParametros(){
  return this.http.get(`${this.urlTp}/parametros/all`)
    .pipe(
      map(this.crearArregloParametros),

    );

}

private crearArregloParametros(paramObj) {

  const parametros: ParametroModel[] = [];

  //console.log('lo que trae del puto array');
  //console.log(categoriasObj);


  if (paramObj === null) { return []; }

  paramObj.data.parametros.forEach((item) => {

    const parametro = new ParametroModel();
    parametro.id = item.id;
    parametro.fechaInicioValidez= item.fechaInicioValidez;
    parametro.fechaFinValidez = item.fechaFinValidez;
    parametro.diasDuracion = item.diasDuracion;

    
    parametros.push(parametro);
  });
    //console.log("desde servicio", clients);
  return parametros;

}

actualizarParametro(idParametro, fechaInicioValidez, fechaFinValidez, diasDuracion){

  const body = {
    "fechaInicioValidez":fechaInicioValidez,
    "fechaFinValidez": fechaFinValidez ,
      "diasDuracion": diasDuracion
    
  };

  return this.http.put(`${this.urlTp}/parametros/edit/${idParametro}`, body)
    .pipe(
      map((resp: any) => {

        console.log(resp);
        return resp;
      })
    );


}

/* cragar puntos bolsa */
cargarPuntos(idCliente, monto){
  const body = {
    "clienteId":idCliente,
    "monto":monto
    
  };
  return this.http.post(`${this.urlTp}/bolsas/add`, body)
    .pipe(
      map((resp: any) => {

        console.log(resp);
        return resp;
      })
    );


}

usarPuntos(idCliente, idConcepto){
  const body = {
    "clienteId":idCliente,
    "conceptoId":idConcepto
    
  };
  return this.http.put(`${this.urlTp}/bolsas/use`, body)
    .pipe(
      map((resp: any) => {

        console.log(resp);
        return resp;
      })
    );

}


consultaMonto(monto){
 return this.http.get(`${this.urlTp}/reglas/equivalencia/${monto}`)
 .pipe(
   map((resp: any) => {

        console.log(resp);
        return resp;
      })
 );
}

usoPuntos(){

  const body = {
    };


  return this.http.post(`${this.urlTp}/consultas/usoPuntos`, body)
    .pipe(
      map(this.crearArregloUsoDetalle),

    );

}

private crearArregloUsoDetalle(usoPuntObj) {

  const puntos: UsoPuntosModel[] = [];

  //console.log('lo que trae del puto array');
  //console.log(categoriasObj);



  if (usoPuntObj === null) { return []; }

  console.log(usoPuntObj);

  usoPuntObj.data.cabeceras.forEach((item) => {

    const usos = new UsoPuntosModel();
    usos.id = item.id;
    usos.puntajeUtilizado= item.puntajeUtilizado;
    usos.fecha = item.fecha;
    usos.usoDetalle = item.usoDetalle;
    usos.vale= item.vale;

    
    puntos.push(usos);
  });
    //console.log("desde servicio", clients);
  return puntos;

}

usoPuntosCliente(idCliente){
  const body = {
     "clienteId": idCliente
  };


return this.http.post(`${this.urlTp}/consultas/usoPuntos`, body)
  .pipe(
    map(this.crearArregloUsoDetalle),

  );


}

usoPuntoVale(idCliente, idConcepto){

  const body = {
    "clienteId": idCliente,
    "valeId": idConcepto
 };


return this.http.post(`${this.urlTp}/consultas/usoPuntos`, body)
 .pipe(
   map(this.crearArregloUsoDetalle),

 );


}

consultarBolsaPuntos(){

  const body = {
  };


return this.http.post(`${this.urlTp}/consultas/bolsaPuntos`, body)
  .pipe(
    map(this.crearArregloBolsa),

  );

}

private crearArregloBolsa(bolsaPuntObj) {

  const bolsaPuntos: BolsaModel[] = [];

  //console.log('lo que trae del puto array');
  //console.log(categoriasObj);



  if (bolsaPuntObj === null) { return []; }

  //console.log(usoPuntObj);

  bolsaPuntObj.data.bolsaPuntos.forEach((item) => {

    const bolsa = new BolsaModel();
    bolsa.id = item.id;
    bolsa.cliente = item.cliente;
    bolsa.puntajeAsignado = item.puntajeAsignado;
    bolsa.puntajeUtilizado = item.puntajeUtilizado;
    
    bolsaPuntos.push(bolsa);
  });
    //console.log("desde servicio", clients);
  return bolsaPuntos;

}

bolsaPuntosCliente(idCliente){

  const body = {
    "clienteId": idCliente
 };


return this.http.post(`${this.urlTp}/consultas/bolsaPuntos`, body)
 .pipe(
   map(this.crearArregloBolsa),

 );

}

buscarVencimiento(dias){
  const body = {
    "dias": dias
 };


return this.http.post(`${this.urlTp}/consultas/vencimientos`, body)
 .pipe(
   map(this.crearArregloClientes),

 );

}







}
