import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgxPaginationModule} from 'ngx-pagination';
import { HttpClientModule } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { PDFExportModule } from '@progress/kendo-angular-pdf-export';
import { HotTableModule } from 'angular-handsontable';




import { AppComponent } from './app.component';
import { ClientesComponent } from './pages/clientes/clientes.component';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { PuntosComponent } from './pages/puntos/puntos.component';
import { MenuComponent } from './pages/menu/menu.component';
import { ClienteAgregarComponent } from './pages/cliente-agregar/cliente-agregar.component';
import { ValesAgregarComponent } from './pages/vales-agregar/vales-agregar.component';
import { ReglaListadoComponent } from './pages/regla-listado/regla-listado.component';
import { ReglaAgregarComponent } from './pages/regla-agregar/regla-agregar.component';
import { ParametrosListadoComponent } from './pages/parametros-listado/parametros-listado.component';
import { ParametrosAgregarComponent } from './pages/parametros-agregar/parametros-agregar.component';
import { BolsasComponent } from './pages/bolsas/bolsas.component';
import { ReportesComponent } from './pages/reportes/reportes.component';
import { UsarPuntoComponent } from './pages/usar-punto/usar-punto.component';
import { ConsultarMontoComponent } from './pages/consultar-monto/consultar-monto.component';
import { ConsultaUsoPuntoComponent } from './pages/consulta-uso-punto/consulta-uso-punto.component';
import { VencimientoComponent } from './pages/vencimiento/vencimiento.component';

import { ExcelService } from './services/excel.service';



@NgModule({
  declarations: [
    AppComponent,
    ClientesComponent,
    PuntosComponent,
    MenuComponent,
    ClienteAgregarComponent,
    ValesAgregarComponent,
    ReglaListadoComponent,
    ReglaAgregarComponent,
    ParametrosListadoComponent,
    ParametrosAgregarComponent,
    BolsasComponent,
    ReportesComponent,
    UsarPuntoComponent,
    ConsultarMontoComponent,
    ConsultaUsoPuntoComponent,
    VencimientoComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    NgxPaginationModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    PDFExportModule,
    HotTableModule
    
    

  ],
  providers: [DatePipe, ExcelService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }
