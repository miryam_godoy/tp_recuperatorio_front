import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientesComponent } from './pages/clientes/clientes.component';
import { PuntosComponent } from './pages/puntos/puntos.component';
import { ClienteAgregarComponent } from './pages/cliente-agregar/cliente-agregar.component';
import { ValesAgregarComponent } from './pages/vales-agregar/vales-agregar.component';
import { ReglaListadoComponent } from './pages/regla-listado/regla-listado.component';
import { ReglaAgregarComponent } from './pages/regla-agregar/regla-agregar.component';
import { ParametrosAgregarComponent } from './pages/parametros-agregar/parametros-agregar.component';
import { ParametrosListadoComponent } from './pages/parametros-listado/parametros-listado.component';
import { BolsasComponent } from './pages/bolsas/bolsas.component';
import { ReportesComponent } from './pages/reportes/reportes.component';
import { UsarPuntoComponent } from './pages/usar-punto/usar-punto.component';
import { ConsultarMontoComponent } from './pages/consultar-monto/consultar-monto.component';
import { ConsultaUsoPuntoComponent } from './pages/consulta-uso-punto/consulta-uso-punto.component';
import { VencimientoComponent } from './pages/vencimiento/vencimiento.component';


const routes: Routes = [
    { path: 'administracion/clientes'   , component: ClientesComponent },
    { path: 'administracion/clientes-agregar'   , component: ClienteAgregarComponent },
    { path: 'administracion/vales-listado'   , component: PuntosComponent },
    { path: 'administracion/vales-agregar'   , component: ValesAgregarComponent },
    { path: 'administracion/regla-listado'   , component: ReglaListadoComponent },
    { path: 'administracion/regla-agregar'   , component: ReglaAgregarComponent },
    { path: 'administracion/parametros-listado'   , component: ParametrosListadoComponent },
    { path: 'administracion/parametros-agregar'   , component: ParametrosAgregarComponent },
    { path: 'operaciones/carga-de-puntos'   , component: BolsasComponent },
    { path: 'operaciones/usar-puntos'   , component: UsarPuntoComponent },
    { path: 'operaciones/consultar-monto'   , component: ConsultarMontoComponent },
    { path: 'reportes/consultar-uso-puntos'   , component: ConsultaUsoPuntoComponent },


    { path: 'reportes/bolsa-puntos'   , component: ReportesComponent },
    { path: 'reportes/vencimiento'   , component: VencimientoComponent},


    { path: '**', redirectTo: 'administracion/clientes' }

]



@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
  })
  export class AppRoutingModule { }