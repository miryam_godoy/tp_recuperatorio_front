import { Component, OnInit } from '@angular/core';
import { ServicioService } from 'src/app/services/servicio.service';
import { ClienteModel } from 'src/app/models/cliente.model';


@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  clientes: ClienteModel[]=[];
  temp: ClienteModel[]=[];
  page = 1;
  cargando = true;

  parametro: string;
  startIndexPage: number;
  pageSize: number;


  constructor( private cliServicio: ServicioService) { }

  ngOnInit() {
    this.cliServicio.getClientes()
    .subscribe(resp =>{
      this.clientes = resp;
      this.temp = resp;
      console.log(resp);
      this.cargando = false;
    })
  }

  paginacion(){
    this.cliServicio.paginarClientes(this.parametro, this.startIndexPage, this.pageSize)
    .subscribe(resp => {
      this.clientes = resp;
      console.log("lo que retorna el sservicio",resp);
    });
  }

  restaurar(){
    this.cargando = true;
    this.ngOnInit();
    this.parametro = undefined;
    this.startIndexPage = undefined;
    this.pageSize = undefined;
  }

  

}
