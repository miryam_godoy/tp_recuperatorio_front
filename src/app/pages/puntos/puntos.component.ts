import { Component, OnInit } from '@angular/core';
import { PuntosModel } from 'src/app/models/puntos.model';
import { ServicioService } from 'src/app/services/servicio.service';

@Component({
  selector: 'app-puntos',
  templateUrl: './puntos.component.html',
  styleUrls: ['./puntos.component.css']
})
export class PuntosComponent implements OnInit {

  vales: PuntosModel[]=[];
  temp: PuntosModel[]=[];
  cargando = true;
  startIndexPage : number;
  pageSize: number;

  constructor( private valeSer: ServicioService) { }

  ngOnInit() {

    this.valeSer.getVales()
    .subscribe(resp =>{
      this.vales = resp;
      this.temp = resp;
      console.log(resp);
      this.cargando = false;
    });
  }

  paginacionVales(){
    console.log("paginado", this.startIndexPage, this.pageSize);
    this.valeSer.paginarVales(this.startIndexPage, this.pageSize)
    .subscribe(resp => {
      this.vales = resp;

    });
  }

  restaurar(){
    this.cargando = true;
    this.ngOnInit()
   // this.vales = this.temp;
    this.startIndexPage = undefined;
    this.pageSize = undefined;
   // this.cargando = true;
  }

}
