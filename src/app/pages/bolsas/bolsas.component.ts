import { Component, OnInit } from '@angular/core';
import { ServicioService } from 'src/app/services/servicio.service';
import { ClienteModel } from 'src/app/models/cliente.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-bolsas',
  templateUrl: './bolsas.component.html',
  styleUrls: ['./bolsas.component.css']
})
export class BolsasComponent implements OnInit {

  monto: number = 0;

  clientes: ClienteModel[] = [];
  tempCliente = new ClienteModel();
  idCliente: number;
  clienteName;


  constructor( private bolsa: ServicioService,
    ) { }

  ngOnInit() {

    this.bolsa.getClientes()
    .subscribe(resp =>{
      this.clientes = resp;
    });
    
  }

  /* seleccionar cliente*/
  seleccionarCliente(item){
    this.tempCliente = this.clientes[item];
   // this.clienteName = `${temp.nombre} ${temp.apellido}`;
    this.idCliente = this.tempCliente.id;
    console.log(this.idCliente);


  }

  /* para cargar el punto */
  cargarPunto( ){

    if( this.clienteName === undefined){

      Swal.fire({
        title: 'Cliente',
        text: 'No fue aun seleccionado un cliente',
        icon: 'warning',
        confirmButtonText: 'Ok'
      });

    }else{

      this.bolsa.cargarPuntos(this.idCliente, this.monto)
    .subscribe(resp =>{
      console.log(resp);
      Swal.fire({
        title: 'Carga de putnos',
        text: 'Puntos cargados exitosamente!!',
        icon: 'success',
        confirmButtonText: 'Ok'
      })
    });

    }






    

  }

  /* limpiar pantalla */
  limpiar(){
    this.monto = 0;
    this.idCliente = undefined;
    this.clienteName = undefined;
  }

  printNameCliente(){
    this.clienteName = `${this.tempCliente.nombre} ${this.tempCliente.apellido}`
  }


  
}
