import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReglaAgregarComponent } from './regla-agregar.component';

describe('ReglaAgregarComponent', () => {
  let component: ReglaAgregarComponent;
  let fixture: ComponentFixture<ReglaAgregarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReglaAgregarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReglaAgregarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
