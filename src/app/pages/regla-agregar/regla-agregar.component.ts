import { Component, OnInit } from '@angular/core';
import { ReglaModel } from 'src/app/models/reglas.model';
import { ServicioService } from 'src/app/services/servicio.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-regla-agregar',
  templateUrl: './regla-agregar.component.html',
  styleUrls: ['./regla-agregar.component.css']
})
export class ReglaAgregarComponent implements OnInit {

  reglas:ReglaModel[]=[];

  monto: number = 0;
  inferior: number;
  superior: number;
  

  constructor( private reglaSer: ServicioService) { }

  ngOnInit() {
  }

  calcularMontoEquivalencia(){
    if( this.inferior !== undefined || this.superior !== undefined){
        if(this.inferior >= 0 && this.superior<= 199999){
            this.monto = this.monto +50000;
            console.log('monto', this.monto);

        }else if(this.inferior >= 200000 && this.superior<= 499999){
          this.monto = this.monto + 30000;
          console.log('monto', this.monto);
        }else if (this.superior >= 500000){
          this.monto = this.monto + 20000;
          console.log('monto', this.monto);

        }
    }
  }

  guardarRegla(){

    if( this.inferior !== undefined || this.superior !== undefined){
      this.reglaSer.setRegla(this.inferior, this.superior, this.monto)
    .subscribe(resp =>{
      console.log(resp);
    });

    Swal.fire({
      title: `El de la regla` ,
      text: 'Se guardo correctamente',
      icon: 'success',
      confirmButtonText: 'Ok'
    });

    }else{
      Swal.fire({
        title: `Error` ,
        text: 'Se debe calcular el monto equivalencia!',
       // type: 'success'
      });
    }
    
  }



}
