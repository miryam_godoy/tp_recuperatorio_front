import { Component, Input, OnInit, Inject} from '@angular/core';
import { ServicioService } from 'src/app/services/servicio.service';
import { ClienteModel } from 'src/app/models/cliente.model';

import Swal from 'sweetalert2';
import * as jsPdf from 'jspdf';
import 'jspdf-autotable';
//import * as jsPdf from 'jspdf';
//import 'jspdf-autotable';
import { ExcelService } from 'src/app/services/excel.service';

declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');
@Component({
  selector: 'app-vencimiento',
  templateUrl: './vencimiento.component.html',
  styleUrls: ['./vencimiento.component.css']
})
export class VencimientoComponent implements OnInit {

  clientes: ClienteModel[]=[];
  cargando = true;
  page = 1;
  mostrarTabla = false;
  dias;
  data: DataVencimientoModel[] = [];

  

  constructor( private consulta: ServicioService,
    private excelService:ExcelService) { }

  ngOnInit() {
    
  }

  buscar(){
    
    if( this.dias !== undefined){
      this.consulta.buscarVencimiento(this.dias)
      .subscribe(resp => {
        this.clientes = resp;
        console.log(resp);
        this.cargando = false;
        this.mostrarTabla = true;

        



      });
    }else{
      Swal.fire({
        title: `Error!` ,
        text: 'No se ingreso los dias',
        icon: 'error',
        confirmButtonText: 'Ok'
      });
    }
  }

  restaurar(){
    
    this.mostrarTabla = false;
    this.dias = undefined;
  }

  imprimirLista(){

    if(this.dias !== undefined){
      const doc = new jsPDF();
    var col = ["Nombre","Apellido", "Fecha Caducidad"];
    var rows = [];
    this.clientes.forEach(element => {      
      var temp = [element.nombre,element.apellido, element.fechaCaducidad];
     // var temp1 = [element.id,element.name];
      rows.push(temp);
     // rows1.push(temp1);

  });  
   
    /*doc.fromHTML(document.getElementById('reporte'),15,15);*/
    doc.autoTable(col, rows, { startY: 10 });

    doc.save('vencimiento-reporte.pdf');

    }else{

      Swal.fire({
        title: `Error!` ,
        text: 'No se ingreso los dias',
        icon: 'error',
        confirmButtonText: 'Ok'
      });

    }
    
  }

  crearData(){
    for(let i= 0; i< this.clientes.length; i++){
      let tempClietes = new ClienteModel();
      let tempData = new DataVencimientoModel();
      tempClietes = this.clientes[i];
      
      tempData.cliente = `${tempClietes.nombre} ${tempClietes.apellido}`;
      tempData.fechaCaducidad = tempClietes.fechaCaducidad;

      this.data.push(tempData);

    }
  }



  

  exportAsXLSX():void {

    if( this.dias !== undefined){
      this.crearData();
    this.excelService.exportAsExcelFile(this.data, 'sample');

    }else{
      Swal.fire({
        title: `Error!` ,
        text: 'No se ingreso los dias',
        icon: 'error',
        confirmButtonText: 'Ok'
      });

    }
    
    
  }

  



}

export class DataVencimientoModel{
 
  cliente:string;
  fechaCaducidad: string;
}
