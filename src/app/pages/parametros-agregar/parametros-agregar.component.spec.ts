import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParametrosAgregarComponent } from './parametros-agregar.component';

describe('ParametrosAgregarComponent', () => {
  let component: ParametrosAgregarComponent;
  let fixture: ComponentFixture<ParametrosAgregarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParametrosAgregarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParametrosAgregarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
