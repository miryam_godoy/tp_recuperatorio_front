import { Component, OnInit } from '@angular/core';
import { ServicioService } from 'src/app/services/servicio.service';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-parametros-agregar',
  templateUrl: './parametros-agregar.component.html',
  styleUrls: ['./parametros-agregar.component.css']
})
export class ParametrosAgregarComponent implements OnInit {

fechaInicio: string;
fechaFin: string;
dias: number;

  constructor(private paramService: ServicioService,
          private datePipe: DatePipe) { }

  ngOnInit() {
  }

   /* convertir fecha */
   transformDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }

  agregarParametro(){

    let fecha1 = this.transformDate(this.fechaInicio);
    let fecha2 = this.transformDate(this.fechaFin);
    this.paramService.setParametro(fecha1, fecha2, this.dias)
    .subscribe(resp =>{
      console.log(resp);
    });

    Swal.fire({
      title: `El parametro` ,
      text: 'Se guardo correctamente!!',
       icon: 'success',
       confirmButtonText: 'Ok'
    });

  }

}
