import { Component, OnInit } from '@angular/core';
import { ServicioService } from 'src/app/services/servicio.service';
import { ReglaModel } from 'src/app/models/reglas.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-regla-listado',
  templateUrl: './regla-listado.component.html',
  styleUrls: ['./regla-listado.component.css']
})
export class ReglaListadoComponent implements OnInit {

  reglas: ReglaModel[]=[];
  cargando = true;

  inferior: number;
  superior: number;
  monto: number;

  idRegla: number;

  reglaBuscar = new ReglaModel();

  constructor( private servReglas: ServicioService) { }

  ngOnInit() {

    this.servReglas.getReglas()
    .subscribe(resp =>{
      this.reglas = resp;
      console.log(resp);
      this.cargando= false;
    })
  }

  buscarRegla(item){
    let temp = new ReglaModel();
    temp = this.reglas[item];
    this.idRegla =  temp.id;
  /*  this.servReglas.buscarRegla(this.idRegla)
    .subscribe(resp =>{
      this.reglaBuscar = resp;
      this.monto = this.reglaBuscar.montoEquivalencia;
    this.inferior = this.reglaBuscar.limiteInferior;
    this.superior = this.reglaBuscar.limiteSuperior;
    console.log('prueba', this.monto);
      console.log(resp);
    });*/
    
   
  }

  actualizarRegla(){
    this.servReglas.actualizarRegla(this.idRegla, this.monto,
      this.inferior, this.superior)
      .subscribe(resp =>{
        console.log(resp);
        this.restaurar();
        Swal.fire({
          title: `El de la regla` ,
          text: 'Se actualizo correctamente',
          icon: 'success',
          confirmButtonText: 'Ok'
        });
    
      });
      
  }

  restaurar(){
    this.cargando = true;
    this.ngOnInit();
    this.monto = undefined;
    this.inferior = undefined;
    this.superior = undefined;

  }
}
