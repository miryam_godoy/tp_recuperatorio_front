import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReglaListadoComponent } from './regla-listado.component';

describe('ReglaListadoComponent', () => {
  let component: ReglaListadoComponent;
  let fixture: ComponentFixture<ReglaListadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReglaListadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReglaListadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
