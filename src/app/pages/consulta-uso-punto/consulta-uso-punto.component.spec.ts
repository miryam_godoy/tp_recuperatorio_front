import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaUsoPuntoComponent } from './consulta-uso-punto.component';

describe('ConsultaUsoPuntoComponent', () => {
  let component: ConsultaUsoPuntoComponent;
  let fixture: ComponentFixture<ConsultaUsoPuntoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultaUsoPuntoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaUsoPuntoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
