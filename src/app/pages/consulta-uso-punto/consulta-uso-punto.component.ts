import { Component, OnInit,  ViewChild, ElementRef, Inject } from '@angular/core';
import { ServicioService } from 'src/app/services/servicio.service';
import { UsoPuntosModel } from 'src/app/models/usoPuntos.model';
import { ClienteModel } from 'src/app/models/cliente.model';
import { PuntosModel } from 'src/app/models/puntos.model';
import Swal from 'sweetalert2';
import { ExcelService } from 'src/app/services/excel.service';



import 'jspdf-autotable';
//import jsPDF from 'jspdf';
//import 'jspdf-autotable';






declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');
@Component({
  selector: 'app-consulta-uso-punto',
  templateUrl: './consulta-uso-punto.component.html',
  styleUrls: ['./consulta-uso-punto.component.css']
})
export class ConsultaUsoPuntoComponent implements OnInit {
  page = 1;
  usoPuntos: UsoPuntosModel[]=[];
  clientes: ClienteModel[]=[];
  vales: PuntosModel[]=[];
  conceptoName;
  tempClientes = new ClienteModel();
  tempVale= new PuntosModel();
  idCliente;
  idConcepto;
  clienteName;

  cargando = true;
  mostrarBusquedaCliente = true;

  data: DataBolsaModel[]=[];

  @ViewChild('content', { 'static': true }) content: ElementRef;

  constructor(private consulta: ServicioService,
    private excelService:ExcelService
    
    ) { }

  ngOnInit() {
    this.consulta.usoPuntos()
    .subscribe(resp =>{
    this.usoPuntos = resp;
    this.cargando = false;
    console.log("Cliente", this.usoPuntos);

    });

    this.consulta.getClientes()
    .subscribe(resp =>{
      this.clientes = resp;
    });

    this.consulta.getVales()
    .subscribe(resp =>{
      this.vales = resp;
    })
  }

  imprimirLista(){
    const doc = new jsPDF();
    var col = ["Concepto de uso","Fecha de uso", "Cliente"];
    var rows = [];
    this.usoPuntos.forEach(element => {  
      let nameTemp = `${element.usoDetalle[0].bolsaPuntos.cliente.nombre} ${element.usoDetalle[0].bolsaPuntos.cliente.apellido}` ;  
      var temp = [element.vale.descripcion, element.fecha, nameTemp];
     // var temp1 = [element.id,element.name];
      rows.push(temp);
     // rows1.push(temp1);

  }); 
  doc.autoTable(col, rows, { startY: 10 });
    doc.save('uso-puntos-reporte.pdf');
  }

  /* para mostrar Barra */
  showBarraSearch(){
    this.mostrarBusquedaCliente = !this.mostrarBusquedaCliente;
    console.log("cambio", this.mostrarBusquedaCliente);
  }

  seleccionarCliente(item){
    this.tempClientes = this.clientes[item];
    this.idCliente = this.tempClientes.id;
    console.log("cliente", this.idCliente);

  }

  buscar(){
   // this.cargando = true;
    this.clienteName = `${this.tempClientes.nombre} ${this.tempClientes.apellido}`;
    this.consulta.usoPuntosCliente(this.idCliente)
    .subscribe(resp =>{
      this.usoPuntos = resp;
      console.log("consulta cliente", this.usoPuntos);
     // this.cargando = false;
    })
  }

  /* seleccionar vale*/
  seleccionarVale(item){
    
    this.tempVale = this.vales[item];
    this.idConcepto = this.tempVale.id;
    console.log("concepto", this.idConcepto);
    //this.conceptoName = this.tempVale.descripcion;
  }


  buscarVale(){
    if( this.idCliente === undefined || this.idConcepto === undefined){
      Swal.fire({
        title: 'Error!',
        text: 'Cliente o concepto no seleccionados',
        icon: 'error',
        confirmButtonText: 'Ok'
      });
    }else{
      this.conceptoName = this.tempVale.descripcion;

      this.consulta.usoPuntoVale(this.idCliente, this.idConcepto)
      .subscribe(resp =>{
        this.usoPuntos = resp;
      });
    }


  }

  restaurar(){
    this.ngOnInit();
    this.idCliente = undefined;
    this.idConcepto = undefined;
    this.clienteName = undefined;
    this.conceptoName = undefined;

  }

  crearData(){
    
    for(let i = 0; i < this.usoPuntos.length; i++){
      let tempPuntos = new UsoPuntosModel();
      let temp = new DataBolsaModel();
      tempPuntos = this.usoPuntos[i];
      //console.log("tempBolsa", tempBolsa);
      //temp.id = tempBolsa.cliente.id;
      temp.cliente = `${tempPuntos.usoDetalle[0].bolsaPuntos.cliente.nombre} ${tempPuntos.usoDetalle[0].bolsaPuntos.cliente.apellido}`;
     // console.log("cliente", temp.nombreCliente);
      temp.conceptoDeUso = tempPuntos.vale.descripcion;
      temp.fechaUso = tempPuntos.fecha;
      this.data.push(temp);
    }

    

    
  }

  exportAsXLSX():void {

    this.crearData();
    
    this.excelService.exportAsExcelFile(this.data, 'sample');
  }

}







export class DataBolsaModel{
  conceptoDeUso: string;
  fechaUso:string;
  cliente: string;
}
