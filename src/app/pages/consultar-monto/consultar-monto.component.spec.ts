import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultarMontoComponent } from './consultar-monto.component';

describe('ConsultarMontoComponent', () => {
  let component: ConsultarMontoComponent;
  let fixture: ComponentFixture<ConsultarMontoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultarMontoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultarMontoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
