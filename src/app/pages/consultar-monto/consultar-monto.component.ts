import { Component, OnInit } from '@angular/core';
import { ServicioService } from 'src/app/services/servicio.service';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-consultar-monto',
  templateUrl: './consultar-monto.component.html',
  styleUrls: ['./consultar-monto.component.css']
})
export class ConsultarMontoComponent implements OnInit {

  monto: number = 0;
  punto;
  noPunto;

  constructor( private consulta: ServicioService) { }

  ngOnInit() {
  }

  /* consultar monto */
  consultarMonto(){
    if(this.monto > 0){
      console.log("monto", this.monto)
      this.consulta.consultaMonto(this.monto)
        .subscribe(resp => {
          if(Object.keys(resp.data).length === 0){
            this.punto = `No existe regla que contenga el monto`;
          }else{

          this.punto = `Se obtiene los puntos exitosamente = ${resp.data.punto}`;
          console.log("punto", this.punto)
          console.log(resp);

          }
          
        });

    }else{

      Swal.fire({
        title: 'Error!',
        text: 'Monto no ingresado correctamente',
        icon: 'error',
        confirmButtonText: 'Ok'
      });

      

    }
     
        
      

      
    
  }

  limpiar(){
    this.monto = 0;
    this.punto = undefined;
  }

}
