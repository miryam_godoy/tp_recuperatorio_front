import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValesAgregarComponent } from './vales-agregar.component';

describe('ValesAgregarComponent', () => {
  let component: ValesAgregarComponent;
  let fixture: ComponentFixture<ValesAgregarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValesAgregarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValesAgregarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
