import { Component, OnInit } from '@angular/core';
import { ServicioService } from 'src/app/services/servicio.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-vales-agregar',
  templateUrl: './vales-agregar.component.html',
  styleUrls: ['./vales-agregar.component.css']
})
export class ValesAgregarComponent implements OnInit {

  descripcion: string;
  cantidadRequerida: number;

  constructor(private valeService: ServicioService) { }

  ngOnInit() {
  }

  guardarVale(){
    this.valeService.setVale(this.descripcion, this.cantidadRequerida)
    .subscribe(resp =>{
      console.log(resp);
    });

    Swal.fire({
      title: `El vale ${this.descripcion}` ,
      text: 'Se guardo correctamente',
      icon: 'success',
      confirmButtonText: 'Ok'
    });
  }

}
