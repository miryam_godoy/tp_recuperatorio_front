import { Component, OnInit } from '@angular/core';
import { ServicioService } from 'src/app/services/servicio.service';
import { ParametroModel } from 'src/app/models/parametro.model';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-parametros-listado',
  templateUrl: './parametros-listado.component.html',
  styleUrls: ['./parametros-listado.component.css']
})
export class ParametrosListadoComponent implements OnInit {

  fechaInicio: Date;
  fechaFin: Date;
  dias: number;

  idParametro: number;
  temp = new ParametroModel();

  parametros: ParametroModel[]=[];
  page = 1;

  cargando = true;
  fecha1;
  fecha2;

  param = new ParametroModel();


  constructor( private paramServ: ServicioService,
    private datePipe: DatePipe) { }

  ngOnInit() {
    this.paramServ.getParametros()
    .subscribe(resp =>{
      this.parametros = resp;
      console.log(resp);
      this.cargando = false;
    })
  }

  /* convertir fecha */
  transformDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }
  transformDate2(datestring) {
    let tempDate  = new Date(datestring);
    let tempString = this.datePipe.transform(tempDate, 'dd/MM/yyyy');
    console.log("fecha1", tempString);
    let temp2 = new Date(tempString);
    return  temp2;
   
  }

  buscarParametro(item){
    let temp1 = new ParametroModel();
    temp1 = this.parametros[item];
    this.idParametro =  temp1.id;
   /* this.paramServ.buscarParametro(this.idParametro)
    .subscribe(resp =>{
      this.param = resp;
     this.fechaInicio = new Date( this.param.fechaInicioValidez);
     console.log('fechaInicio', this.fechaInicio);
      this.dias = this.param.diasDuracion;
      
    })*/
    
  }

  actualizarParametro(){
    let fecha1 = this.transformDate(this.fechaInicio);
    let fecha2 = this.transformDate(this.fechaFin);
    this.paramServ.actualizarParametro(this.idParametro, fecha1, fecha2, this.dias)
    .subscribe(resp =>{
      console.log(resp);
      this.restaurar();
    });

    Swal.fire({
      title: `El parametro` ,
      text: 'Se actualizo correctamente',
      icon: 'success',
      confirmButtonText: 'Ok'
    });
   
  }

  restaurar(){
    this.cargando = true;
    this.ngOnInit();
    this.fechaInicio = undefined;
    this.fechaFin = undefined;
    this.dias = undefined;
    

  }

}
