import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParametrosListadoComponent } from './parametros-listado.component';

describe('ParametrosListadoComponent', () => {
  let component: ParametrosListadoComponent;
  let fixture: ComponentFixture<ParametrosListadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParametrosListadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParametrosListadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
