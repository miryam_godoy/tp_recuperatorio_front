import { Component, OnInit } from '@angular/core';
import { ServicioService } from 'src/app/services/servicio.service';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-cliente-agregar',
  templateUrl: './cliente-agregar.component.html',
  styleUrls: ['./cliente-agregar.component.css']
})
export class ClienteAgregarComponent implements OnInit {

  nombre: string;
  apellido: string;
  nroDocumento: string;
  email: string;
  telefono: string;
  fechaNacimiento: Date;

  constructor( private cliService: ServicioService,private datePipe: DatePipe) { }

  ngOnInit() {
  }
  /* convertir fecha */
  transformDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }

  guardarClienteNuevo(){
    console.log(this.nombre);
    console.log(this.nroDocumento);
    let fecha = this.transformDate(this.fechaNacimiento);
    this.cliService.setCliente(this.nombre, this.apellido,
      this.nroDocumento, this.email, this.telefono, fecha)
      .subscribe(resp => {
        console.log(resp);
      });

      Swal.fire({
        title: `El cliente ${this.nombre}` ,
        text: 'Se guardo correctamente',
         icon: 'success',
         confirmButtonText: 'Ok'
      });
        
      
  }

}
