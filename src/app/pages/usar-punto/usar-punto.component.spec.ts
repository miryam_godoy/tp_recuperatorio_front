import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsarPuntoComponent } from './usar-punto.component';

describe('UsarPuntoComponent', () => {
  let component: UsarPuntoComponent;
  let fixture: ComponentFixture<UsarPuntoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsarPuntoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsarPuntoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
