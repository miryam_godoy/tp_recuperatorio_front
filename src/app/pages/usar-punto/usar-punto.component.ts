import { Component, OnInit } from '@angular/core';
import { ServicioService } from 'src/app/services/servicio.service';
import { ClienteModel } from 'src/app/models/cliente.model';
import { PuntosModel } from 'src/app/models/puntos.model';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-usar-punto',
  templateUrl: './usar-punto.component.html',
  styleUrls: ['./usar-punto.component.css']
})
export class UsarPuntoComponent implements OnInit {

  clientes: ClienteModel[]=[];
  tempClientes = new ClienteModel();
  vales: PuntosModel[]=[];
  tempVale= new PuntosModel();
  clienteName;
  idCliente;
  idConcepto;
  conceptoName;

  constructor(private punto: ServicioService) { }

  ngOnInit() {
    this.punto.getClientes()
    .subscribe(resp =>{
      this.clientes = resp;
    });

    this.punto.getVales()
    .subscribe(resp =>{
      this.vales = resp;
    })
  }

  /* seleccionar vale*/
  seleccionarVale(item){
    
    this.tempVale = this.vales[item];
    this.idConcepto = this.tempVale.id;
    console.log("concepto", this.idConcepto);
    //this.conceptoName = this.tempVale.descripcion;
  }

  /* imprimir el vale elgido */
  printValeDesc(){
    this.conceptoName = this.tempVale.descripcion;
  }

  /* seleccionar cliente */
  seleccionarCliente(item){
    this.tempClientes = this.clientes[item];
    this.idCliente = this.tempClientes.id;
    console.log("cliente", this.idCliente);
    
  }

  /* mostrar cliente en pantalla */
  printClienteSel(){
    this.clienteName = `${this.tempClientes.nombre} ${this.tempClientes.apellido}`;
  }

  limpiar(){
    this.clienteName = undefined;
    this.conceptoName = undefined;
    this.idCliente = undefined;
    this.idConcepto = undefined;
  }

  /* utilizar puntos **/
  agregar(){
    this.punto.usarPuntos(this.idCliente, this.idConcepto)
    .subscribe(resp => {
      //console.log(resp.data);
      if(Object.keys(resp.data).length === 0){
         Swal.fire({
        title: 'Error!',
        text: 'El cliente no tiene los puntos necesarios',
        icon: 'error',
        confirmButtonText: 'Ok'
      });
      }else if(resp.data.bolsas === null){

        Swal.fire({
        title: 'Carga de puntos',
        text: 'No existe la bolsa asociada con el cliente ingresado',
        icon: 'warning',
        confirmButtonText: 'Ok'
      });

      }else{
        Swal.fire({
        title: 'Uso de puntos',
        text: 'Se creo exitosamente la bolsa',
        icon: 'success',
        confirmButtonText: 'Ok'
      });
      }
      

    });
  }



}
