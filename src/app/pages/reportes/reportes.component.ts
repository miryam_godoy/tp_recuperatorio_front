import { Component, OnInit } from '@angular/core';
import { ServicioService } from 'src/app/services/servicio.service';
import { BolsaModel } from 'src/app/models/bolsa.model';
import { ClienteModel } from 'src/app/models/cliente.model';
import { ExcelService } from 'src/app/services/excel.service';
import * as jsPdf from 'jspdf';
import 'jspdf-autotable';

declare const require: any;
const jsPDF = require('jspdf');
require('jspdf-autotable');
@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css']
})
export class ReportesComponent implements OnInit {

  bolsaPuntos : BolsaModel[]=[];
  clientes: ClienteModel[]=[];
  tempClientes = new ClienteModel();
  idCliente;
  page =1;
  cargando = true;
  clienteName;

  data: DataModel[]=[];


  constructor( private consulta: ServicioService,
    private excelService:ExcelService) { }

  ngOnInit() {
    this.consulta.consultarBolsaPuntos()
    .subscribe(resp =>{
      this.bolsaPuntos = resp;
      this.cargando = false;
    });
    this.consulta.getClientes()
    .subscribe(resp =>{
      this.clientes = resp;
    });
  }

  seleccionarCliente(item){
    this.tempClientes = this.clientes[item];
    this.idCliente = this.tempClientes.id;
    console.log("cliente", this.idCliente);

  }

  imprimirLista(){
    const doc = new jsPDF();
    var col = ["Cliente","Puntos Asignados", "Puntos Utilizados"];
    var rows = [];
    this.bolsaPuntos.forEach(element => {  
      let nameTemp = `${element.cliente.nombre} ${element.cliente.apellido}` ;  
      var temp = [nameTemp,element.puntajeAsignado, element.puntajeUtilizado];
     // var temp1 = [element.id,element.name];
      rows.push(temp);
     // rows1.push(temp1);

  }); 
   doc.autoTable(col, rows, { startY: 10 });
    doc.save('uso-puntos-reporte.pdf');
  }


  buscar(){
    // this.cargando = true;
     this.clienteName = `${this.tempClientes.nombre} ${this.tempClientes.apellido}`;
     this.consulta.bolsaPuntosCliente(this.idCliente)
     .subscribe(resp =>{
       this.bolsaPuntos = resp;
       console.log("consulta cliente", this.bolsaPuntos);
      // this.cargando = false;
     });
   }

   restaurar(){
    this.ngOnInit();
    this.idCliente = undefined;
   
    this.clienteName = undefined;
   

  }

  crearData(){
    
    for(let i = 0; i < this.bolsaPuntos.length; i++){
      let tempBolsa = new BolsaModel();
      let temp = new DataModel();
      tempBolsa = this.bolsaPuntos[i];
      //console.log("tempBolsa", tempBolsa);
      temp.id = tempBolsa.cliente.id;
      temp.cliente = `${tempBolsa.cliente.nombre} ${tempBolsa.cliente.apellido}`;
     // console.log("cliente", temp.nombreCliente);
      temp.puntosAsignado = tempBolsa.puntajeAsignado;
      temp.puntajeUtilizado = tempBolsa.puntajeUtilizado;
      this.data.push(temp);
    }

    

    
  }

  exportAsXLSX():void {

    this.crearData();
    
    this.excelService.exportAsExcelFile(this.data, 'sample');
  }

}

export class DataModel{
  id: number;
  cliente: string;
  puntosAsignado: number;
  puntajeUtilizado: number;
}
