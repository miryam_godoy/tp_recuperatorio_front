import { PuntosModel } from './puntos.model';
import { UsoDetalleModel } from './usoDetalle.model';

export class UsoPuntosModel{
    id: number;
    puntajeUtilizado: number;
    fecha: string;
    usoDetalle: UsoDetalleModel;
    vale: PuntosModel;
   
}