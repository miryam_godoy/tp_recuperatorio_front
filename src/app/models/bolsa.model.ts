import { ClienteModel } from './cliente.model';


export class BolsaModel{
    id: number;
   cliente: ClienteModel;
   fechaAsignacion: string;
   fechaCaducidad: string;
   puntajeAsignado: number
   puntajeUtilizado: number;
   saldo: number;
   monto: number;
   estado: number;
}