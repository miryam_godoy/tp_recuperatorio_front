export class ParametroModel{
    id: number;
    fechaInicioValidez: string;
    fechaFinValidez: string;
    diasDuracion: number;
}