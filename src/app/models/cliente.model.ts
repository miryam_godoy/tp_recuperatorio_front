export class ClienteModel{
    id: number;
    nombre: string;
    apellido : string;
    nroDocumento : string;
    email: string;
    telefono: string;
    fechaNacimiento: string;
    fechaCaducidad: string;
}