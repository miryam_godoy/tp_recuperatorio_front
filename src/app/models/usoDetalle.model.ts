import { BolsaModel } from './bolsa.model';

export class UsoDetalleModel{
    id: number;
    puntajeUtilizado: number;
    bolsaPuntos: BolsaModel;
    
}